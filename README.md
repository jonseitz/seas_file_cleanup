#SEAS File cleanup

A Drupal 7 module the move and update files loaded to the wrong location on the SEAS website.

Creates a new page at `/admin/config/content/filecleanup` with an option for a dry run. Outputs a list of cleaned files at the end.
